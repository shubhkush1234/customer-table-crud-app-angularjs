var app= angular.module('myApp',[]);
app.controller('myController', function($scope){

	$scope.location=["India","Pakistan","Nepal","USA","Australia","UAE","Japan","New Zealand"] ;
   $scope.customerDetailList= {
			details: [{
				name:"",
				mailId:"",
				
        }]};
  
		$scope.add = function(){
				$scope.customerDetailList.details.push({
				name:$scope.name,
				selectedLocation:$scope.selectedLocation,
				mailId:$scope.mailId
			});
				window.localStorage.setItem('customerDetailsApp',JSON.stringify($scope.customerDetailList.details));
				$scope.reset();		
		};
    
  		$scope.reset = function(){
			$scope.name="";
			$scope.selectedLocation="";
			$scope.mailId="";
		}
		$scope.delete = function(index){
			$scope.tempList = [];
			$scope.customerDetailList.details.splice(index, 1);
			if(window.localStorage.getItem('customerDetailsApp'))
			{
				$scope.tempList= JSON.parse(window.localStorage.getItem('customerDetailsApp'));
				$scope.tempList.splice(index, 1);
				window.localStorage.setItem('customerDetailsApp',JSON.stringify($scope.tempList));
			}
		
		};	

		$scope.myJson={
			name:'',
			selectedLocation:'',
			mailId:'',
		};
		
		if(window.localStorage.getItem('customerDetailsApp'))
		{
			$scope.customerDetailList.details= JSON.parse(window.localStorage.getItem('customerDetailsApp'));
		}

		
 		$scope.buttonName="Click To Sort";

	    $scope.change=function(){
			if($scope.buttonName=="DESC"){
				$scope.sortList = "-name";
				$scope.buttonName="ASC";
			}
			else{
				$scope.sortList="name" ;
				$scope.buttonName="DESC";
			}
		
	};

});